import { add_image_sneaker } from './../modals/add_image_sneaker';
import { Component, OnInit } from '@angular/core';
import { ImagenesService } from './../services/imagenes.service';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';


@Component({
  selector: 'app-armario',
  templateUrl: './armario.page.html',
  styleUrls: ['./armario.page.scss'],
})
export class ArmarioPage implements OnInit {

  sneaker:add_image_sneaker[];

  constructor(private router:Router, 
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService) { }

  ngOnInit() {
  }

  OnClickAddSneaker(){
    this.router.navigateByUrl("add-sneaker");
  }

  ionViewDidEnter(){
    let sneakers_consulta = this.imagenesservice.getSneaker();
    sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
  }

  ampliar_sneaker(item:add_image_sneaker){
    console.log("hola");
    this.router.navigate(['ampliar-sneaker', {add_image_sneaker: JSON.stringify(item)}]);
    console.log(JSON.stringify(item));
  }

}
