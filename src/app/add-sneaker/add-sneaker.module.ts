import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddSneakerPageRoutingModule } from './add-sneaker-routing.module';

import { AddSneakerPage } from './add-sneaker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddSneakerPageRoutingModule
  ],
  declarations: [AddSneakerPage]
})
export class AddSneakerPageModule {}
