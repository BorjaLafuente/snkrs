import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { image } from '../modals/image';
import { add_image } from '../modals/add_image';

@Component({
  selector: 'app-ampliar-foto',
  templateUrl: './ampliar-foto.page.html',
  styleUrls: ['./ampliar-foto.page.scss'],
})
export class AmpliarFotoPage implements OnInit {
  img:image = {
    id: "",
    url: ""
  }

  foto:add_image = {
    id: "",
    piedefoto: "",
    img: this.img,
    id_usuario: ""
  }

  pie_de_foto:string="";
  
  constructor(private route:ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.foto = JSON.parse(params.imagen);
      this.pie_de_foto = this.foto.piedefoto;
      this.img = this.foto.img;
  }); 

   }

  ngOnInit() {
  }

}
