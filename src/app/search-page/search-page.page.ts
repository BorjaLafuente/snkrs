import { usuario_firebase } from '../modals/usuario_firebase';
import { UserService} from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.page.html',
  styleUrls: ['./search-page.page.scss'],
})
export class SearchPagePage implements OnInit {
followers:any;
buscar:string;
  constructor(private router:Router, private userservice:UserService) { 
    this.buscar="";
  }

  ngOnInit() {
   
  }

  ionViewWillEnter(){
     this.userservice
    .GetFollowers()
    .then((data) => {
      data.valueChanges().subscribe((res) => {
        this.followers = res;
      });
    })
    .catch();
   
  }

  handleInput(event) {
    console.log(event);
    const searchbar = document.querySelector('ion-searchbar');
    const items = Array.from(document.getElementById("lista").children);
    console.log(items);
  
    const query = this.buscar.toLowerCase();
    console.log(query);
    requestAnimationFrame(() => {
      items.forEach(item => {
        let shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
        //item.style.display = shouldShow ? 'block' : 'none';
        if (shouldShow){
         item.classList.add("poner_block");
         item.classList.remove("poner_none");
        }else{
          item.classList.add("poner_none");
          item.classList.remove("poner_block");
        }

      });
    });
  }

  enter_usuario(item:usuario_firebase){
    this.router.navigate(['users-profiles', {usuario_firebase:JSON.stringify(item),array_usuario:JSON.stringify(this.followers)}]);
    console.log(item);
  }
  
}
