import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {

  constructor(private afAuth:AngularFireAuth,private toastService:ToastController) { }

  
  async createuser(mail:string,pass:string){
    return this.afAuth.createUserWithEmailAndPassword(mail,pass);
  }

  signup(mail:string,pass:string){
    return this.afAuth.signInWithEmailAndPassword(mail,pass)
  }

  async presentToast(message:string) {
    const toast = await this.toastService.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
