import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmpliarFotoPageRoutingModule } from './ampliar-foto-routing.module';

import { AmpliarFotoPage } from './ampliar-foto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmpliarFotoPageRoutingModule
  ],
  declarations: [AmpliarFotoPage]
})
export class AmpliarFotoPageModule {}
