import { add_image } from './../modals/add_image';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-app',
  templateUrl: './home-app.page.html',
  styleUrls: ['./home-app.page.scss'],
})
export class HomeAppPage implements OnInit {

  listFollowers: any;
  listImages: Array<add_image>;
  comprobacion: any;

  constructor(private userservice:UserService) { 
    this.listImages = [];
  }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.listImages = [];
    this.userservice
      .GetFollowing()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.listFollowers = res.reverse();
        });
      })
      .catch();
    this.userservice
      .GetImages_global()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.comprobacion = res;
          if (this.comprobacion != undefined && this.comprobacion != null) {
            this.listFollowers.forEach((followers) => {
              this.comprobacion.forEach((imagenes) => {
                if (imagenes.id_usuario == followers.id) {
                  this.listImages.push(imagenes);
                  console.log(this.listImages);
                }
              });
            });
          }
        });
      })
      .catch();


  }


}
