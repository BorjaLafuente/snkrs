import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmpliarFotoPage } from './ampliar-foto.page';

describe('AmpliarFotoPage', () => {
  let component: AmpliarFotoPage;
  let fixture: ComponentFixture<AmpliarFotoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliarFotoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmpliarFotoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
