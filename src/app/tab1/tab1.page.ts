import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import {AuthenticationServiceService } from '../services/authentication-service.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { usuario_firebase } from 'src/app/modals/usuario_firebase';
import * as firebase from 'firebase';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private authservices:AuthenticationServiceService,private router:Router, public loadingController: LoadingController,private userServices:UserService) {}
  textUsername:string = "";
  textPassword:string = "";
  textMessage:string="";
  passwordTypeInput:string="password"
  user_firebase:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
  };

  ionViewWillEnter(){
    console.log("hola");
    this.textUsername="";
    this.textPassword="";
  }

  boton(){
    
  if(this.textUsername.trim().length> 0 && this.textPassword.trim().length > 0){
      this.authservices.signup(this.textUsername,this.textPassword)
      .then((newUserCredential: firebase.auth.UserCredential) => {
        let user_login = this.userServices.getUser(newUserCredential.user.uid);
        user_login.valueChanges().subscribe(res=>{
          this.authservices.presentToast("Usuario logeado correctamente");
          this.router.navigateByUrl('tabs-main/home_app');
          sessionStorage.setItem("user",JSON.stringify(res));
          this.presentLoading();
        })
      },error => {
        if(error.message.includes("user") || error.message.includes("email")){
          this.authservices.presentToast("El usuario es incorrecto");
        }

        if(error.message.includes("password")){
          this.authservices.presentToast("El password es incorrecto");
        }
        
      });
    }else{
        this.textMessage="Formulario Incorrecto";
    }

  }

  email(){
    if(this.textUsername.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
  
  }
  
    password(){
    if(this.textPassword.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
  
  }

  async onclickcreateaccount(){
    this.router.navigateByUrl("tabs/tab3");
    }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  togglepassword(){
    console.log("password");
    if(this.passwordTypeInput=="password"){
      this.passwordTypeInput="text"
    }else{
      this.passwordTypeInput="password"
    }
  }

}
