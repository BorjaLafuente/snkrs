import { UserService } from './../services/user.service';
import { usuario_firebase } from '../modals/usuario_firebase';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { add_image } from '../modals/add_image';

@Component({
  selector: 'app-users-profiles',
  templateUrl: './users-profiles.page.html',
  styleUrls: ['./users-profiles.page.scss'],
})
export class UsersProfilesPage implements OnInit {

  usuario:usuario_firebase = {
    id: "",
    mail: "",
    user: "",
  }

  imagenes:any;

    id:string = "";
    mail:string = "";
    user:string = "";

    imagen:add_image[];
    followers:any;

  constructor(private route:ActivatedRoute, private userservice:UserService, private router:Router, ) { 
    this.route.params.subscribe(params=>{
    console.log(params);
    this.usuario = JSON.parse(params['usuario_firebase']);
    this.followers = JSON.parse(params['array_usuario']);
    this.id = this.usuario.id;
    this.mail = this.usuario.mail;
    this.user = this.usuario.user;  
    }); 
  }

  ngOnInit() {
  
  }

  ionViewWillEnter(){
    this.userservice
   .GetImages(this.id)
   .then((data) => {
     data.valueChanges().subscribe((res) => {
       this.imagenes = res;
       console.log(res);
     });
     
   })
   .catch();
  
 }

 makefollow(id){
   console.log(id);
   this.followers.forEach(element => {
     console.log(element);
     if(id==element.id){
      this.userservice.follow(element);
     }
   });
 }

 openmessage(){
   console.log("Open Message");

 }

 openwardrobe(id){
  console.log("Open Wardrobe");
  this.router.navigate(['ampliar-wardrobe', {wardrobe: JSON.stringify({id})}]);
  console.log(JSON.stringify(id));
 }

 ampliarfoto(item){
  console.log("hola");
  this.router.navigate(['ampliar-foto', {imagen: JSON.stringify(item)}]);
  console.log(JSON.stringify(item));
}


}
