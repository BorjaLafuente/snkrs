import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddSneakerPage } from './add-sneaker.page';

describe('AddSneakerPage', () => {
  let component: AddSneakerPage;
  let fixture: ComponentFixture<AddSneakerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSneakerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddSneakerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
