import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersProfilesPageRoutingModule } from './users-profiles-routing.module';

import { UsersProfilesPage } from './users-profiles.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsersProfilesPageRoutingModule
  ],
  declarations: [UsersProfilesPage]
})
export class UsersProfilesPageModule {}
