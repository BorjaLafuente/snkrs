import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmpliarSneakerPageRoutingModule } from './ampliar-sneaker-routing.module';

import { AmpliarSneakerPage } from './ampliar-sneaker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmpliarSneakerPageRoutingModule
  ],
  declarations: [AmpliarSneakerPage]
})
export class AmpliarSneakerPageModule {}
