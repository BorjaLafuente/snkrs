import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoadingController, ActionSheetController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ImagenesService } from './../services/imagenes.service';
import { image_sneaker } from './../modals/image_sneaker';
import { add_image_sneaker } from './../modals/add_image_sneaker';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-add-sneaker',
  templateUrl: './add-sneaker.page.html',
  styleUrls: ['./add-sneaker.page.scss'],
})
export class AddSneakerPage implements OnInit {

  constructor( private storage: AngularFireStorage,
    private db: AngularFirestore,private camera: Camera,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    public actionSheetController: ActionSheetController,
    private imagePicker: ImagePicker,
    private router:Router) { }

    img_sneaker:image_sneaker = {
      id: "",
      url: ""
    }
  
    add_sneaker:add_image_sneaker = {
      id: "",
      nombre: "",
      marca: "",
      modelo: "",
      img_sneaker: this.img_sneaker,
      id_usuario: ""
    }
  
    sneaker_name = "";
    sneaker_brand = "";
    sneaker_model = "";

    imageResponse: any;
    options1: any;
    base64Image = '';

  ngOnInit() {
  }

  onClickAddImage(){
    if(this.sneaker_name != "" && this.sneaker_brand != "" && this.sneaker_model != ""){
      console.log("HOLA");
      this.add_sneaker.nombre = this.sneaker_name;
      this.add_sneaker.marca = this.sneaker_brand;
      this.add_sneaker.modelo = this.sneaker_model;
      this.add_sneaker.img_sneaker = this.img_sneaker;
      this.imagenesservice.addimages_sneaker(this.add_sneaker);
      this.imagenesservice.presentToast("Image uploaded successfully");
      this.router.navigateByUrl('tabs-main/home_app');
      this.add_sneaker.nombre = "";
      this.add_sneaker.marca = "";
      this.add_sneaker.modelo = "";
      this.img_sneaker.url = "";
    }
  }

  onClickcamera() {
    console.log("button pressed");
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });
      await loading.present();
      this.img_sneaker.id = this.db.createId();
      let route = `/${this.img_sneaker.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');

      task.snapshotChanges().pipe(
        finalize(() => {
          alert("hola");
          fileRef.getDownloadURL().subscribe(url => {
            this.img_sneaker.url = url;
            alert(this.img_sneaker.url);
            loading.dismiss();
          });
        })
      ).subscribe();

    }, (err) => {
      alert(err);
      // Handle error
    });;
  }


  onClickgallery() {
    alert("button pressed");
    let options1 = {
      maximumImagesCount: 1,
      width: 400,
      height: 400, 
      quality: 25,
      outputType: 1
    };
    this.imageResponse = [];
    this.imagePicker.getPictures(options1).then(async (results) => {
    alert("linea 114");
      this.base64Image = 'data:image/jpeg;base64,' + results;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });
      await loading.present();
      this.img_sneaker.id = this.db.createId();
      let route = `/${this.img_sneaker.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            this.img_sneaker.url = url;
            alert(this.img_sneaker.url);
            loading.dismiss();
          });
        })
      ).subscribe();
    }, (err) => {
      alert(err);
    });
  }



  async OnClickImg() {


    const actionSheet = await this.actionSheetController.create({
      header: 'Select an option',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.onClickcamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          this.onClickgallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
