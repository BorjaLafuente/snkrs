import { ImagenesService } from './../services/imagenes.service';
import { Component, OnInit } from '@angular/core';
import { image } from './../modals/image';
import { add_image } from './../modals/add_image';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController, ActionSheetController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import * as firebase from 'firebase';

@Component({
  selector: 'app-upload-images',
  templateUrl: './upload-images.page.html',
  styleUrls: ['./upload-images.page.scss'],
})
export class UploadImagesPage implements OnInit {

  img:image = {
    id: "",
    url: ""
  }

  imagen_añadida:add_image = {
    id: "",
    piedefoto: "",
    img: this.img,
    id_usuario: ""
  }

  caption="";
  imageResponse: any;
  options1: any;
  base64Image = '';


  constructor(private imagePicker: ImagePicker,
    private router:Router, private storage: AngularFireStorage,
    private db: AngularFirestore,private camera: Camera,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    public actionSheetController: ActionSheetController) { }

  ngOnInit() {
  }

  onClickAddImage(){
    if(this.caption != ""){
      console.log("HOLA");
      this.imagen_añadida.piedefoto = this.caption;
      this.imagen_añadida.img = this.img;
      this.imagen_añadida.id_usuario = JSON.parse(sessionStorage.getItem("user")).id;
      console.log(this.imagen_añadida.id_usuario);
      this.imagenesservice.addimages(this.imagen_añadida);
      this.imagenesservice.presentToast("Image uploaded successfully");
      this.router.navigateByUrl('tabs-main/home_app');
      this.caption = "";
      this.img.url = "";
    }
  }

  onClickcamera() {
    console.log("button pressed");
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });
      await loading.present();
      this.img.id = this.db.createId();
      let route = `/${this.img.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');

      task.snapshotChanges().pipe(
        finalize(() => {
          alert("hola");
          fileRef.getDownloadURL().subscribe(url => {
            this.img.url = url;
            alert(this.img.url);
            loading.dismiss();
          });
        })
      ).subscribe();

    }, (err) => {
      alert(err);
      // Handle error
    });;
  }
  
  onClickgallery() {
    alert("button pressed");
    let options1 = {
      maximumImagesCount: 1,
      width: 200,
      height: 200, 
      quality: 100,
      outputType: 1
    };

    this.imagePicker.getPictures(options1).then(async (results) => {
    alert("linea 114");
    let storage = firebase.storage();
    let storageRef = storage.ref();
    var desertRef = storageRef.child(this.img.id);
    desertRef.delete();

      this.base64Image = 'data:image/jpeg;base64,' + results;
      console.log(this.base64Image);

        if(results.length!=0){
        const loading = await this.loadingCtrl.create({
        message:"Guardando foto..."
        });

        await loading.present();

      this.img.id = this.db.createId();
      let route = `/${this.img.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            this.img.url = url;
            alert(this.img.url);
            loading.dismiss();
          });
        })
      ).subscribe();
    }
    });
  }


  async OnClickImg() {


    const actionSheet = await this.actionSheetController.create({
      header: 'Select an option',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.onClickcamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          this.onClickgallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
