import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAppPage } from './home-app.page';

describe('HomeAppPage', () => {
  let component: HomeAppPage;
  let fixture: ComponentFixture<HomeAppPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAppPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAppPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
