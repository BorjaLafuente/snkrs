import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmpliarWardrobePage } from './ampliar-wardrobe.page';

const routes: Routes = [
  {
    path: '',
    component: AmpliarWardrobePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmpliarWardrobePageRoutingModule {}
