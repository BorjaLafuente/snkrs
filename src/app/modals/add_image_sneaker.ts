import { image_sneaker } from './image_sneaker';
export interface add_image_sneaker{
    id: string;
    nombre: string;
    marca: string;
    modelo: string;
    img_sneaker: image_sneaker;
    id_usuario: string;
}
