import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'armario',
    loadChildren: () => import('./armario/armario.module').then( m => m.ArmarioPageModule)
  },
  {
    path: 'search-page',
    loadChildren: () => import('./search-page/search-page.module').then( m => m.SearchPagePageModule)
  },
  {
    path: 'upload-images',
    loadChildren: () => import('./upload-images/upload-images.module').then( m => m.UploadImagesPageModule)
  },
  {
    path: 'tabs-main',
    loadChildren: () => import('./tabs-main/tabs-main.module').then( m => m.TabsMainPageModule)
  },
  {
    path: 'home-app',
    loadChildren: () => import('./home-app/home-app.module').then( m => m.HomeAppPageModule)
  },  {
    path: 'add-sneaker',
    loadChildren: () => import('./add-sneaker/add-sneaker.module').then( m => m.AddSneakerPageModule)
  },
  {
    path: 'ampliar-sneaker',
    loadChildren: () => import('./ampliar-sneaker/ampliar-sneaker.module').then( m => m.AmpliarSneakerPageModule)
  },
  {
    path: 'users-profiles',
    loadChildren: () => import('./users-profiles/users-profiles.module').then( m => m.UsersProfilesPageModule)
  },
  {
    path: 'ampliar-foto',
    loadChildren: () => import('./ampliar-foto/ampliar-foto.module').then( m => m.AmpliarFotoPageModule)
  },
  {
    path: 'ampliar-wardrobe',
    loadChildren: () => import('./ampliar-wardrobe/ampliar-wardrobe.module').then( m => m.AmpliarWardrobePageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
