import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {AuthenticationServiceService } from '../services/authentication-service.service';
import { ToastController } from '@ionic/angular';
import { AngularFirestore} from '@angular/fire/firestore'
import { user } from 'src/app/modals/user'
import { UserService } from '../services/user.service';
import { usuario_firebase } from '../modals/usuario_firebase';
import * as firebase from 'firebase';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  textEmail:string;
  textPassword:string;
  textUser:string;
  constructor(private userServices:UserService,private db:AngularFirestore, private router:Router,private authservices:AuthenticationServiceService,private toastService:ToastController) {
    this.textEmail="";
    this.textPassword="";
    this.textUser="";
    this.textMessage="";
  }
  textMessage:string="";
  user_firebase:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
  };

  email(){
    if(this.textEmail !==undefined && this.textEmail !== ""){
      this.textMessage="";
    }else{
      this.textMessage="Email Incorrecto";
    }
  }

  user(){
    if(this.textUser !==undefined || this.textUser !== ""){
      this.textMessage="";
    }else{
      this.textMessage="Email Incorrecto";
    }
  }
  
    password(){
      if(this.textPassword !==undefined || this.textPassword !== ""){
        this.textMessage="";
      }else{
        this.textMessage="Email Incorrecto";
      }
    }

  saveregister(){
    console.log("prueba");

      console.log(this.textEmail);
      this.authservices.createuser(this.textEmail,this.textPassword)
      .then((newUserCredential: firebase.auth.UserCredential) => {
        console.log("hola");
        this.authservices.presentToast("Usuario creado correctamente");
        this.user_firebase.mail=this.textEmail;
        this.user_firebase.id=newUserCredential.user.uid;
        this.user_firebase.user=this.textUser;
        this.userServices.createUser(this.user_firebase);
        this.router.navigateByUrl("tabs/tab1");
      },error => {
        if(error.message.includes("The email address is already in use by another account.")){
          this.authservices.presentToast("Este usuario ya existe");
        }
        if(error.message.includes("The email address is badly formatted.")){
          this.authservices.presentToast("La dirección de correo electrónico está mal formateada");
        }
        if(error.message.includes("Password should be at least 6 characters")){
          this.authservices.presentToast("La contraseña debe tener al menos 6 caracteres");
        }

      });
    }

}
