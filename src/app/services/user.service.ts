import { usuario_firebase } from './../modals/usuario_firebase';
import { element } from 'protractor';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { add_image } from '../modals/add_image';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private db:AngularFirestore) { }

  createUser(user_firebase:usuario_firebase){

    this.db.doc('users/'+user_firebase.id).set(user_firebase).then(res=>{

    }).catch(err=>{
      throw err;
    });
    }
    
    public getUser(id:string){
      return this.db.doc<usuario_firebase>("users/"+id);
    }

    async GetFollowers() {
      let data: AngularFirestoreCollection = this.db.collection<usuario_firebase>(
        "users"
      );
      return data;
    }

    async GetFollowing() {
      let user_info = JSON.parse(sessionStorage.getItem("user"));
      let data: AngularFirestoreCollection = this.db.collection<usuario_firebase>(
        "users/"+user_info.id+"/following"
      );
      return data;
    }

    async GetImages(id:string) {
      let id_usuario = id;
      let data: AngularFirestoreCollection = this.db.collection<add_image>(
        "users/"+id_usuario+"/images"
      );
      return data;
    }

    async GetImages_global() {
      let data: AngularFirestoreCollection = this.db.collection<add_image>(
        "images"
      );
      return data;
    }

    async follow(element:usuario_firebase) {
     let user_info = JSON.parse(sessionStorage.getItem("user"));
     this.db.doc('users/'+user_info.id+"/following/"+element.id).set(element);
    }
      
    
    
}
