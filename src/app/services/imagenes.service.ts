import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { image } from './../modals/image';
import { image_sneaker } from './../modals/image_sneaker';
import { add_image } from './../modals/add_image';
import { add_image_sneaker } from './../modals/add_image_sneaker';

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  constructor(private db:AngularFirestore,private toast:ToastController) { }

  addimages(add_image:add_image){
    let id_users=JSON.parse(sessionStorage.getItem("user"));
    add_image.id=this.db.createId();
    this.db.doc("users/"+id_users.id+"/images/"+add_image.id).set(add_image);
    this.db.doc("images/"+id_users.id).set(add_image);
  }

  addimages_sneaker(add_image_sneaker:add_image_sneaker){
    let id_users=JSON.parse(sessionStorage.getItem("user"));
    add_image_sneaker.id=this.db.createId();
    this.db.doc("users/"+id_users.id+"/sneakers/"+add_image_sneaker.id).set(add_image_sneaker);
    console.log("users/"+id_users.id+"/sneakers/"+add_image_sneaker.id);
  }

  getSneaker(){
    let id_users=JSON.parse(sessionStorage.getItem("user"));
    return this.db.collection<any>("users/"+id_users.id+"/sneakers/");
  }

  getSneakerUser(id){
    let id_usuario = id;
    console.log(id_usuario);
    console.log(this.db.collection<any>("users/"+id_usuario+"/sneakers/"));
    return this.db.collection<any>("users/"+id_usuario+"/sneakers/");

  }



  async presentToast(msg: string, ok = false, duration = 2000) {
    const toast = await this.toast.create({
      message: msg,
      duration: ok ? null : duration,
      position: 'bottom',
    });
    toast.present();
  }

  
}
