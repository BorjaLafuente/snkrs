import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmpliarWardrobePageRoutingModule } from './ampliar-wardrobe-routing.module';

import { AmpliarWardrobePage } from './ampliar-wardrobe.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmpliarWardrobePageRoutingModule
  ],
  declarations: [AmpliarWardrobePage]
})
export class AmpliarWardrobePageModule {}
