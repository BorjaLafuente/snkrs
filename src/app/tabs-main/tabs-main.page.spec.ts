import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabsMainPage } from './tabs-main.page';

describe('TabsMainPage', () => {
  let component: TabsMainPage;
  let fixture: ComponentFixture<TabsMainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsMainPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabsMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
